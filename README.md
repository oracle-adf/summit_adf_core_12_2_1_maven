# Summit-ADF

http://www.oracle.com/technetwork/developer-tools/jdev/learnmore/index-098948.html


    # yum install -y git

    # su - oracle12

    $ cd /tmp/
    $ git clone https://github.com/oracle-adf/Summit-ADF

    $ cd /tmp/Summit-ADF/SummitADF_Schema1221/Database/scripts


    $ sqlplus system/<password>

    SQL> spool result.txt
    SQL> @build_summit_schema.sql;
    SQL> spool off;
    SQL> exit


This script creates user: summit_adf


<br/>

### Generating Maven pom.xml


New --> From Gallery

![Application](/img/maven1.png?raw=true)

<br/>

![Application](/img/maven2.png?raw=true)
